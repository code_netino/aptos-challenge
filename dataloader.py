import torch
import h5py
from torchvision import transforms
import numpy as np
import torch
import pandas as pd
from torch.utils.data import DataLoader
from torchvision import transforms
import torch.utils.data as data
from PIL import Image
import os
import cv2
import glob
from custom_transforms import Sujithscale


class DRfull(data.Dataset):

    def __init__(self, images_dir,tfms=None):
        super(DRfull, self).__init__()
        self.img_dir=images_dir
        self.csv_file='train_data.csv'
        csv_pd=pd.read_csv(self.csv_file)
        self.imgs=csv_pd.iloc[:,0]
        self.imgs_path=[]
        self.tfms=tfms
        self.lbs=csv_pd.iloc[:,1]
        self.ls=list(self.lbs)
        self.mlimages=[]
        self.mllabels=[]
        for j in range(len(self.imgs)):
            if('_' in self.imgs[j]):
                self.mlimages.append(os.path.join(self.img_dir+'/'+str(self.imgs[j])+'.jpeg'))
            else:
                self.mlimages.append(os.path.join(self.img_dir+'/'+str(self.imgs[j])+'.png'))
            self.mllabels.append(self.lbs[j])
    def __getitem__(self, index): 
        mlimg=cv2.imread(self.mlimages[index])
        aa=cv2.resize(mlimg,(256,256))
        if self.tfms is not None:
            image=self.tfms(aa)
        mul_label=np.array(self.mllabels[index])
        mul_label_torch=torch.from_numpy(mul_label)  
        return (image,mul_label_torch)
    def __len__(self):
        return len(self.mllabels)

if __name__ == "__main__":
   root_path='/home/sujith/aptos2019-blindness-detection/train_data'
   transformations = transforms.Compose([Retinascale(300),transforms.ToTensor()])
   dset_train = DRfull(root_path,transformations)
