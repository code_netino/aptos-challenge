import torch
import h5py
import numpy as np
import torch
import pandas as pd
from torch.utils.data import DataLoader
from torchvision import transforms
import torch.utils.data as data
from PIL import Image
import os
import random
import cv2
import scipy.ndimage as ndi
import glob
from math import floor,ceil
import matplotlib.pyplot as plt


class Retinascale(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, scale):
        self.scale=scale
    def __call__(self, sample):
        #img = cv2.imread(sample)  
        sample = cv2.cvtColor(sample, cv2.COLOR_BGR2RGB)  
        #print(sample.ndim)
        sample=crop_image_from_gray(sample)
        b=np.zeros(sample.shape)
        cv2.circle(b,(sample.shape[1]//2,sample.shape[0]//2),int(self.scale*0.9),(1,1,1),-1,8,0)
        aa=cv2.addWeighted(sample,4,cv2.GaussianBlur(sample,(0,0),self.scale/30),-4,128)*b+128*(1-b)
        aa=cv2.resize(aa,(256,256))
        aa=(aa).astype(np.uint8)
        return aa

class Zoom(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self,min_factor,max_factor):
        self.min_factor,self.max_factor=min_factor,max_factor
    def __call__(self, image):
        factor = round(random.uniform(self.min_factor, self.max_factor), 2)

#         def do(image):
        w, h = image.size
        #print(image.size)

        image_zoomed = image.resize((int(round(image.size[0] * factor)),
                                         int(round(image.size[1] * factor))),
                                         resample=Image.BICUBIC)
        w_zoomed, h_zoomed = image_zoomed.size

        return image_zoomed.crop((floor((float(w_zoomed) / 2) - (float(w) / 2)),
                                      floor((float(h_zoomed) / 2) - (float(h) / 2)),
                                      floor((float(w_zoomed) / 2) + (float(w) / 2)),
                                      floor((float(h_zoomed) / 2) + (float(h) / 2))))


def crop_image1(img,tol=7):
    # img is image data
    # tol  is tolerance
        
    mask = img>tol
    return img[np.ix_(mask.any(1),mask.any(0))]

def crop_image_from_gray(img,tol=7):
    if img.ndim ==2:
        mask = img>tol
        return img[np.ix_(mask.any(1),mask.any(0))]
    elif img.ndim==3:
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        mask = gray_img>tol
        
        check_shape = img[:,:,0][np.ix_(mask.any(1),mask.any(0))].shape[0]
        if (check_shape == 0): # image is too dark so that we crop out everything,
            return img # return original image
        else:
            img1=img[:,:,0][np.ix_(mask.any(1),mask.any(0))]
            img2=img[:,:,1][np.ix_(mask.any(1),mask.any(0))]
            img3=img[:,:,2][np.ix_(mask.any(1),mask.any(0))]
            img = np.stack([img1,img2,img3],axis=-1)
    #         print(img.shape)
        return img

