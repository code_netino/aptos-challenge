import timeit
from torchvision import transforms
from custom_transforms import Retinascale,Zoom
from datetime import datetime
import socket
import math
import os
import glob
from tqdm import tqdm
import torchvision.models as models
import torch
import numpy as np
import random
from torch import nn, optim
from fcn import *
import PIL
from apex import amp
from torch.utils.data import DataLoader
from torch.autograd import Variable
from feat_model import feature_extract
from efficientnet_pytorch import EfficientNet
from dataloader import DRfull




def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True


def cyclical_lr(stepsize, min_lr=3e-4, max_lr=3e-3):

    # Scaler: we can adapt this if we do not want the triangular CLR
    scaler = lambda x: 1.

    # Lambda function to calculate the LR
    lr_lambda = lambda it: min_lr + (max_lr - min_lr) * relative(it, stepsize)

    # Additional function to see where on the cycle we are
    def relative(it, stepsize):
        cycle = math.floor(1 + it / (2 * stepsize))
        x = abs(it / stepsize - 2 * cycle + 1)
        return max(0, (1 - x)) * scaler(cycle)

    return lr_lambda


def mixup_data(x, y, alpha=1.0, use_cuda=True):
    '''Returns mixed inputs, pairs of targets, and lambda'''
    if alpha > 0:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1

    batch_size = x.size()[0]
    if use_cuda:
        index = torch.randperm(batch_size).cuda()
    else:
        index = torch.randperm(batch_size)

    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam


def mixup_criterion(criterion, pred, y_a, y_b, lam):
    return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)

def train_model(train_all,test_all,lr,num_epochs):    
    
    seed_everything(1234)

    # Loading the data
    train_loader_all=DataLoader(dataset=train_all,batch_size=8,shuffle=True,num_workers=1)
    val_loader_all=DataLoader(dataset=test_all,batch_size=8,shuffle=True,num_workers=1)

    # Importing Effnet
    model=EfficientNet.from_name('efficientnet-b5') 
    model.load_state_dict(torch.load('/home/sujith/aptos2019-blindness-detection/efficientnet-b5-586e6cc6.pth'))
    model._fc=nn.Linear(in_features=2048, out_features=1, bias=True)
    
    # MSE loss
    criterion =  nn.MSELoss()

    # Pushing the Model to GPU
    model.to(device)
    criterion.to(device)
    
    # Adam Optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=1e-5)

    # LR scheduler
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)

    # Mixed precision training init
    model, optimizer = amp.initialize(model, optimizer, opt_level="O1",verbosity=0)

    # Training
    coef = [0.5, 1.5, 2.5, 3.5]
    print('Total params: %.2fM' % (sum(p.numel() for p in model.parameters()) / 1000000.0))
    
    trainval_loaders = {'train': train_loader_all, 'val': val_loader_all}
    trainval_sizes = {x: len(trainval_loaders[x].dataset) for x in ['train', 'val']}

    train_pred = np.zeros((len(train_loader_all.dataset), 1))
    test_pred = np.zeros((len(val_loader_all.dataset), 1))
    for epoch in range(0, num_epochs):
        for phase in ['train', 'val']:
            start_time = timeit.default_timer()

            running_loss = 0.0
            running_corrects = 0.0

            if phase == 'train':

                scheduler.step()
                model.train()
                optimizer.zero_grad()
            else:
                model.eval()
            for inputs,labels in tqdm((trainval_loaders[phase])):
                inputs,labels=inputs.float(),labels.float()
                labels=labels.view(labels.shape[0],-1)
                inputs = Variable(inputs, requires_grad=True).to(device)
                labels = Variable(labels).to(device)
                if phase == 'train':        
                    outputs = model(inputs)
                else:
                    with torch.no_grad():
                        outputs = model(inputs)
                loss = criterion(outputs,labels)
                if phase == 'train':
                    with amp.scale_loss(loss, optimizer) as scaled_loss:
                         scaled_loss.backward()
                    optimizer.step()
                    optimizer.zero_grad()
                # Scaling down the predictions to Kappa Coefficients 
                for i, pred in enumerate(outputs):
                     if pred < coef[0]:
                         outputs[i] = 0
                     elif pred >= coef[0] and pred < coef[1]:
                         outputs[i] = 1
                     elif pred >= coef[1] and pred < coef[2]:
                         outputs[i] = 2
                     elif pred >= coef[2] and pred < coef[3]:
                         outputs[i] = 3
                     else:
                         outputs[i] = 4

                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(labels == outputs.float())

            epoch_loss = running_loss / trainval_sizes[phase]
            epoch_acc = running_corrects.double() / trainval_sizes[phase]
            print("[{}] Epoch: {}/{} Loss: {} Accuracy:{}".format(phase, epoch+1, nEpochs, epoch_loss,epoch_acc))
            stop_time = timeit.default_timer()
            print("Execution time: " + str(stop_time - start_time) + "\n")
    torch.save(model.state_dict(),'/home/sujith/aptos2019-blindness-detection/bal256e13b16.pt')
def main():
    # Applying Transformations to the Image
    transformations = transforms.Compose([Retinascale(300),transforms.ToPILImage(),transforms.RandomHorizontalFlip(),transforms.RandomRotation((-120, 120)),transforms.ToTensor(),transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))])

    # Calling the dataloader Class
    dset_all=DRfull(images,transformations)

    # Splitting the data
    train_size_all = int(0.8 * len(dset_all))
    test_size_all=len(dset_all)-train_size_all
    train_dataset_all,test_dataset_all=torch.utils.data.random_split(dset_all,[train_size_all,test_size_all])
    lr = 1e-3
    train_model(train_dataset_all,test_dataset_all,lr,nEpochs)
if __name__ == "__main__":
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Device being used:", device)
    nEpochs = 30
    images='/home/sujith/aptos2019-blindness-detection/train_data'
    main()
