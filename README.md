# APTOS Kaggle Challenge

This is the Implementation of my solution as a part of team paarvAI for the APTOS Kaggle Challenge. The end goal of this challenge was to build an effective fundus Image classifier that would classify the given fundus Image to 5 different classses in relevance to the degree of diabetic retinopathy presence found in that Image. The 4 classes are 1.) No diabetic retinopathy 2.) Mild diabetic retinopathy 3.) Severe diabetic retinopathy 4.) Proliferative diabetic retinopathy. Initially, a lot custom  CNN architectures from scratch and also prominent CNN architectures such as ResNet, DenseNet,Inception and EfficientNet were used to extract the predictions. After multiple experimentations on various architectures , the [EfficientNet](https://arxiv.org/abs/1905.11946) architecture was narrowed down as the backbone architecture for the solution on the basis of the promising results obtained. For the generation of the final results for the challenge, an array of model boosting techniques such as mixed precison training, progressive resizing , hyperparameter tuning etc were employed. Image Preprocessing techniques such as scaling, horizontal flipping, rotation and normalization were also done prior to feeding the images to the model. The final submission is an ensemble of multiple model CSV outputs on the basis of the max voting principle.The public leaderboard score for the final submission was 0.803142 and a private leaderboard score of 0.912409 thus placing us at a global rank of 292/2931 teams(Top 10%) and securing the **bronze medal**.

- train.py - Implementation of Model calling,Training,Model boosting and Local evaluation on the given data.  
- dataloader.py - Implementation of the dataloader script to feed the data(Images) to the model accordingly. 
- custom_transforms.py - Script consisting of the transform functions such as Scaling,Zooming and Cropping.
- submission_notebook.ipynb - The final submission_notebook for the challenge.

Some of the libraries used for this Implementation:-

- Pytorch 
- Numpy
- Apex
- PIL
- OpenCV 
- Matplotlib
- Pandas

[Final private leaderboard standings](https://www.kaggle.com/c/aptos2019-blindness-detection/leaderboard). Team name: **paarvAI(292/2931)**


### Applications

This final solution was imparted as a major classification pipeline for multiple classification based projects at HTIC, IIT-MADRAS.
